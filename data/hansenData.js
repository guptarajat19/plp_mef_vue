export default [
    {
        Name: 'O2 my prepaid L',
        ID: '221f727e-91a7-47ae-b5cf-7c0191fbd1e4',
        Business_ID: '000024',
        Marketing_Price: '19.99',
        Marketing_Recurring_Price: '19.99',
        Marketing_Description:
            'Mobile life is fun with the O2 my Prepaid L Tariff. Thanks to Allnet Flat, you can make calls to all German networks as much as you want. And use the mobile internet with 12 GB and LTE Max. Without a contract term.',
        Marketing_short_description:
            '12GB LTE Max data | Allnet-Flat on all German networks | EU Roaming included | 25 € cash-back if you take your number with you',
        Contract: 'false',
        Product_name_suffix: 'L',
        Product_name_prefix: 'O2 My Prepaid',
        Allowance_Duration: '4 weeks',
        Data_Allowance: '12GB',
    },
    {
        Name: 'Blau Spike Test Offer',
        ID: '2a7fae80-f9e0-4b26-baed-faf6d9f575cc',
        Business_ID: '000039',
        Marketing_Price: '19.99',
        Marketing_Recurring_Price: '19.99',
        Marketing_Description:
            'Mobile life is fun with the O2 my Prepaid L Tariff. Thanks to Allnet Flat, you can make calls to all German networks as much as you want. And use the mobile internet with 12 GB and LTE Max. Without a contract term.',
        Marketing_short_description: 'SPIKE Offer to test',
        Contract: 'false',
        Product_name_suffix: 'Spike',
        Product_name_prefix: 'Blau',
        Allowance_Duration: '4 weeks',
        Data_Allowance: '12GB',
    },
    {
        Name: 'O2 my prepaid M',
        ID: '403df60f-c776-4e5f-b6ea-6333217cd2e4',
        Business_ID: '000025',
        Marketing_Price: '14.99',
        Marketing_Recurring_Price: '14.99',
        Marketing_Description:
            'Mobile life is fun with the O2 my Prepaid M Tariff. Thanks to Allnet Flat, you can make calls to all German networks as much as you want. And use the mobile internet with 6.5 GB and LTE Max. Without a contract term.',
        Marketing_short_description:
            '6.5GB LTE Max data | Allnet-Flat on all German networks | EU Roaming included | 25 € cash-back if you take your number with you',
        Contract: 'false',
        Product_name_suffix: 'M',
        Product_name_prefix: 'O2 My Prepaid',
        Allowance_Duration: '4 weeks',
        Data_Allowance: '6.5GB',
    },
    {
        Name: 'O2 my prepaid S',
        ID: 'eb15bb49-378c-4e6a-8b95-2f706c16e74d',
        Business_ID: '000020',
        Marketing_Price: '9.99',
        Marketing_Recurring_Price: '9.99',
        Marketing_Description:
            'Mobile life is fun with the O2 my Prepaid S Tariff. Thanks to Allnet Flat, you can make calls to all German networks as much as you want. And use the mobile internet with 3.5 GB and LTE Max. Without a contract term.',
        Marketing_short_description:
            '3.5GB LTE Max data | Allnet-Flat on all German networks | EU Roaming included | 25 € cash-back if you take your number with you',
        Contract: 'false',
        Product_name_suffix: 'S',
        Product_name_prefix: 'O2 My Prepaid',
        Allowance_Duration: '4 weeks',
        Data_Allowance: '3.5GB',
    },
];
