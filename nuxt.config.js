export default {
    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        title: 'tef-poc',
        htmlAttrs: {
            lang: 'en',
        },
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: '' },
        ],
        link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    },

    router: {
        extendRoutes(routes, resolve) {
            routes.push({
                name: 'custom',
                path: '*',
                component: resolve(__dirname, 'pages/index.vue'),
            });
        },
    },
    globals: {
        id: (globalName) => '_idOffersNuxtJs',
    },
    build: {
        publicPath: '/offersNuxtJs/_nuxt/',
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [
        // https://go.nuxtjs.dev/eslint
        // '@nuxtjs/eslint-module'
    ],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/axios',
        '@nuxtjs/proxy',
    ],

    // Axios module configuration: https://go.nuxtjs.dev/config-axios
    axios: {
        proxy: true,
    },
    proxy: {
        '/cs3': {
            target:
                'https://hansen-dev-catalogservices.low.tefde-aws-raitt01-test.aws.de.pri.o2.com',
            changeOrigin: true,
        },
    },
    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {},
};
